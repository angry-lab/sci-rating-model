package rating

type Input struct {
	Experience         int `json:"experience"`
	CountOfPublic      int `json:"count_of_public"`
	Citation           int `json:"citation"`
	ForeignPublication int `json:"foreign_publication"`
	Position           int `json:"position"`
}

type Output struct {
	Prediction []float64 `json:"prediction"`
}
