#!/usr/bin/env python3

import json
import logging
import pickle
from http.server import BaseHTTPRequestHandler, HTTPServer

import pandas as pd


class Server(BaseHTTPRequestHandler):
    def __init__(self, request, client_address, server):
        path = './model.pt'
        self.model = pickle.load(open(path, 'rb'))
        BaseHTTPRequestHandler.__init__(self, request, client_address, server)

    def _predict(self, experience, count_of_public, citation, foreign_publication, position):
        if count_of_public == 0:
            citat_by_count = 0
        else:
            citat_by_count = citation / count_of_public

        sample = {'experience': [experience], 'count_of_public': [count_of_public], 'citation': [citation],
                  'foreign_publication': [foreign_publication], 'position': [position],
                  'citat_by_count': [citat_by_count]}
        df = pd.DataFrame(sample)
        pred = self.model.predict(df)
        return pred

    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        self._set_response()
        self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))

    def do_POST(self):
        logging.info("POST request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        length = int(self.headers.get('content-length'))
        req = json.loads(self.rfile.read(length))
        pred = self._predict(req['experience'], req['count_of_public'], req['citation'], req['foreign_publication'], req['position'])
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(json.dumps({'prediction': pred.tolist()}).encode())


def run(server_class=HTTPServer, handler_class=Server, port=8800):
    logging.basicConfig(level=logging.INFO)
    server_address = ('0.0.0.0', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')


run()
