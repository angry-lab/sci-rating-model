package rating

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

type Client struct {
	http.Client

	Scheme string
	Host   string
	Port   uint16
}

func (c Client) getURL() string {
	return fmt.Sprintf("%s://%s:%d/", c.Scheme, c.Host, c.Port)
}

func (c *Client) GetRating(input Input) (*Output, error) {
	b, err := json.Marshal(input)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, c.getURL(), bytes.NewReader(b))
	if err != nil {
		return nil, err
	}

	resp, err := c.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	output := Output{}

	err = json.NewDecoder(resp.Body).Decode(&output)
	if err != nil {
		return nil, err
	}

	return &output, nil
}
