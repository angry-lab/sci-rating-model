FROM amancevice/pandas:1.1.4-slim

COPY . /serve
WORKDIR /serve

RUN apt-get update -y && apt-get install -y gfortran libopenblas-dev liblapack-dev
RUN pip3 install scipy sklearn

EXPOSE 8800

ENTRYPOINT ["python3", "./main.py"]
